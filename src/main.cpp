#include <QtQuick>
#include <QGuiApplication>
#include <sailfishapp.h>

#include "videofilter.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<VideoFilter>("VideoFilter", 1, 0, "VideoFilter");

    QGuiApplication *app = SailfishApp::application(argc, argv);
    QQuickView *view = SailfishApp::createView();

    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    return app->exec();
}
