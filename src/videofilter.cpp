#include "videofilter.h"
#include <QFuture>
#include <QtConcurrent>

#include <QDebug>

class VideoFilterRunnable : public QVideoFilterRunnable {
public:
    explicit VideoFilterRunnable(VideoFilter *filter);
    ~VideoFilterRunnable() override;

    QVideoFrame run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags) override;

private:
    static void analyze(VideoFilterRunnable *runnable);

    VideoFilter *m_filter = nullptr;
    QFuture<void> m_future;
    uchar *m_bits = nullptr;
    QImage m_img;
};

VideoFilterRunnable::VideoFilterRunnable(VideoFilter *filter)
    : m_filter(filter) {}

VideoFilterRunnable::~VideoFilterRunnable() {
    if (m_future.isRunning()) {
        m_future.waitForFinished();
    }

    if (m_bits) {
        delete m_bits;
        m_bits = nullptr;
    }

    m_img = QImage();
    m_filter = nullptr;
}

QVideoFrame VideoFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags) {
    Q_UNUSED(flags)
    if (!input || !input->isValid()) {
        qWarning() << "Invalid input video frame!";
        return *input;
    }

    if (m_future.isRunning()) {
        qWarning() << "Thread is busy.";
        return *input;
    }

    if (input->map(QAbstractVideoBuffer::ReadOnly)) {
        int mappedSize = input->mappedBytes();
        uchar *pBits = input->bits();

        if (!pBits) {
            input->unmap();
            return *input;
        }

        if (m_bits) {
            delete m_bits;
        }

        m_bits = new uchar[mappedSize];
        memcpy(m_bits, pBits, mappedSize);
        m_img = QImage(m_bits, surfaceFormat.frameWidth(), surfaceFormat.frameHeight(), QImage::Format_ARGB32);

        m_future = QtConcurrent::run(analyze, this);

        input->unmap();
    }

    return *input;
}

void VideoFilterRunnable::analyze(VideoFilterRunnable *runnable) {
    qDebug() << "Inside the thread.";
}

VideoFilter::VideoFilter(QObject *parent) : QAbstractVideoFilter(parent) {}

VideoFilter::~VideoFilter() {}

QVideoFilterRunnable *VideoFilter::createFilterRunnable() {
    return new VideoFilterRunnable(this);
}
