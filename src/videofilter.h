#ifndef VIDEOFILTER_H
#define VIDEOFILTER_H

#include <QObject>
#include <QAbstractVideoFilter>

class VideoFilter : public QAbstractVideoFilter
{
    Q_OBJECT
    friend class VideoFilterRunnable;
public:
    explicit VideoFilter(QObject *parent = nullptr);
    virtual ~VideoFilter();

    QVideoFilterRunnable *createFilterRunnable();
};

#endif // VIDEOFILTER_H
