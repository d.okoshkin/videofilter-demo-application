TARGET = videofilter-application

CONFIG += sailfishapp

QT += multimedia concurrent

SOURCES += \
    src/main.cpp \
    src/videofilter.cpp

DISTFILES += qml/videofilter-application.qml \
    qml/cover/CoverPage.qml \
    qml/pages/MainPage \
    rpm/videofilter-application.spec \
    translations/*.ts \
    videofilter-application.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/videofilter-application-ru.ts

HEADERS += \
    src/videofilter.h
