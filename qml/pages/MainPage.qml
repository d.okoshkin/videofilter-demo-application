import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.6
import VideoFilter 1.0

Page {
    id: page

    allowedOrientations: Orientation.Portrait

    VideoFilter {
        id: videoFilter
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        source: camera
        autoOrientation: false
        filters: [ videoFilter ]
    }

    Camera {
        id: camera

        position: Camera.BackFace
        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }
        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash
        focus.focusMode: CameraFocus.FocusContinuous
    }
}
